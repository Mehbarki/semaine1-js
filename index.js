/************************************************************** START CONSTANTES ********************************************************************************/

const chars = {
    'letters' : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    'numbers' : '0123456789',
    'specials' : '!"#$%&\'()*+,-.'
}
const limit = 30;
const btnPwd = document.getElementById('btn-password');
const btnCelcius = document.getElementById('btn-celcius');
const btnFaran = document.getElementById('btn-faran');
const btnDialog = document.getElementById('btn-citation');
const btnMode = document.getElementById('btn-darkmode');
const btnMenu = document.getElementById('btn-menu');
const list = document.getElementById('list');

/************************************************************** END CONSTANTES ********************************************************************************/

/************************************************************** START CALLBACK ********************************************************************************/

const printPassword = () => {
    let p = document.getElementById('password');
    if(p) {
        p.remove();
    }
    let password = generatePassword(limit);
    p = document.createElement('p');
    p.innerText = password;
    p.id = 'password';
    btnPwd.after(p);
}

const convertFaranheit = () => {
    let celcius = Number(document.getElementById('celcius').value);
    if(typeof celcius === 'number') {
        let faran = ((9/5) * celcius) + 32;
        putDegres(faran, 'res-celcius', btnCelcius);
    } else {
        alert('Erreur : pas de type number')
    }
}

const convertCelcius = () => {
    let faran = Number(document.getElementById('faran').value);
    if(typeof faran === 'number') {
        let celcius = (faran - 32) / 1.8;
        putDegres(celcius, 'res-faran', btnFaran);
    } else {
        alert('Erreur : pas de type number')
    }
}

const printDialog = () => {
    let dialog = document.getElementById('citation');
    if(dialog) {
        alert(dialog.value)
    }
}

const changeTheme = () => {
    let theme = document.getElementById('current-theme');
    applyTheme(theme);
}

const toggleBurgerMenu = () => {
    let infos = document.getElementById('printMenu');

    if(infos.value === 'close') {
        list.classList.remove('show-off');
        list.classList.toggle('show-menu'); 
        infos.value = 'print'
    } else {
        list.classList.remove('show-menu');
        list.classList.toggle('show-off'); 
        infos.value = 'close'
    }
}

/************************************************************** END CALLBACK ********************************************************************************/

/************************************************************** START EVENTS ********************************************************************************/

btnPwd.addEventListener('click', printPassword);
btnCelcius.addEventListener('click', convertFaranheit);
btnFaran.addEventListener('click', convertCelcius);
btnDialog.addEventListener('click', printDialog);
btnMode.addEventListener('click', changeTheme);
btnMenu.addEventListener('click', toggleBurgerMenu);

/************************************************************** END EVENTS ****************************************************************************/

/************************************************************** START PASSWORD ******************************************************************************/
   
/**
 * 
 * @param {Integer} limit The limit of the password
 * @returns {String} Return the random password 
 */
function generatePassword(limit) {
    let password = '';
    if(limit) {
        while(password.length <= limit) {
            let random = this.getRandom();
            password += this.chooseChar(random)
        }
        // We add 3 chars by 3 so if we want a limit we need to check after the loop the lenght of the password
        if(password.length > limit) {
            let diff = password.length - limit;
            password = password.substring(0, password.length - diff)
        }
    } else {
        return 'You need to pass one argument to the function';
    }

    return password;
}

/**
 * 
 * @returns {Array} Return a random array between 0 and max length for each property of object chars
 */

function getRandom() { 
    return [Math.floor(Math.random() * chars.letters.length), Math.floor(Math.random() * chars.numbers.length), Math.floor(Math.random() * chars.specials.length)];
}

/**
 * Choose character foreach property from Object chars with the random array
 * @param {Array} random 
 * @returns {String} Return 3 chars foreach property of object chars
 */
function chooseChar(random) {
    return chars.letters[random[0]] + chars.numbers[random[1]] + chars.specials[random[2]];
}

/************************************************************** END PASSWORD ********************************************************************************/

/************************************************************** START DEGRES ******************************************************************************/
   
function putDegres(res, id, btn) {
    let currentRes = document.getElementById(id);
    if(currentRes) {
        currentRes.remove();
    }
    let input = document.createElement('input');
    input.value = Math.floor(res);
    input.id = id;
    input.style.marginLeft = '5px';
    btn.after(input);
   }

/************************************************************** END DEGRES ********************************************************************************/

/************************************************************** START THEME ******************************************************************************/
   
function applyTheme(theme) {
    if(theme.value === 'normal') {
        document.querySelector('body').style.background = 'black';
        document.querySelector('body').style.color = 'white';

        let inputs = document.getElementsByTagName('input');
        for(i=0;i<inputs.length;i++)
        {
            inputs[i].style.backgroundColor = 'black';
            inputs[i].style.color = 'white';
        }

        let buttons = document.getElementsByTagName('button');
        for(i=0;i<buttons.length;i++)
        {
            buttons[i].style.backgroundColor = 'black';
            buttons[i].style.color = 'white';
        }

        theme.value = 'dark';
    }else {
        document.querySelector('body').style.background = 'white';
        document.querySelector('body').style.color = 'black';

        let inputs = document.getElementsByTagName('input');
        for(i=0;i<inputs.length;i++)
        {
            inputs[i].style.backgroundColor = 'white';
            inputs[i].style.color = 'black';
        }

        let buttons = document.getElementsByTagName('button');
        for(i=0;i<buttons.length;i++)
        {
            buttons[i].style.backgroundColor = '#e7e7e7';
            buttons[i].style.color = 'black';
        }
        theme.value = 'normal';
    }
    
}

/**************************************************************  END THEME ********************************************************************************/
